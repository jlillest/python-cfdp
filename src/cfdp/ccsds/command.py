from .header import CcsdsHeader, CCSDS_CMD_PKT_TYPE

#
# CCSDS Packet Length = total packet length - 7. 
#    Command packets have 8 bytes of header so add 1 to payload length
#
class CcsdsCommand(CcsdsHeader):
    '''
    /*----- CCSDS command secondary header. -----*/

    typedef struct {

        uint16  Command;      /* command secondary header */
            /*  bits  shift   ------------ description ---------------- */
            /* 0x00FF    0  : checksum, calculated by ground system     */
            /* 0x7F00    8  : command function code                     */
            /* 0x8000   15  : reserved, set to 0                        */

    } CCSDS_CmdSecHdr_t;
    '''
    
    def __init__(
            self,
            app_id=None,
            func_code=None,
            payload=None):
        
        if None in [app_id, func_code]:
            raise Exception("Must provide all required parameters")
        
        self.func_code = func_code
        self.checksum  = self.compute_checksum()
        self.pkt_len   = 0 if payload is None else (1 + len(payload))
        self.payload   = payload
        
        super().__init__(
            app_id=app_id,
            sec_hdr=1,
            pkt_type=CCSDS_CMD_PKT_TYPE,
            pkt_len=self.pkt_len)


    def encode(self):
        # TODO - Make portable endian solution
        databytes = super().encode()
        databytes += bytes([
            (self.func_code & 0x7F),
            (self.checksum & 0xFF)])
        if self.payload is not None:
            databytes += self.payload 
        #print("Command::encode()-databytes after command encode:" + str(databytes))
        
        return databytes

    @classmethod
    def decode(cls, pdu):
        pass
        
    def compute_checksum(self):
        return 0

