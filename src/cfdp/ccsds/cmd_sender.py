import socket


class HexCmd():
    '''
    '''

    def __init__(self, name, data):
        
        self.name = name
        self.data = self.add_checksum(data)
        
            
    def add_checksum(self, data):
        '''
        Compute and insert the checksum for an array of command bytes. 
        '''
        data[6] = 0x00
        checksum = 0xFF
        for hex_byte in data:
            checksum = checksum ^ hex_byte
    
        data[6] = checksum
        
        return data
        
        
class CmdManager:
    '''
    Send hardcoded commands to a cFS target
    ''' 

    cmd_dict = {}
    
    def __init__(self, host_addr, cmd_port):
    
        self.cmd_ip_address = (host_addr, cmd_port) 
        
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        except:
            print("Error creating CmdSender socket")


    def add_cmd(self, hex_cmd: HexCmd):
        cmd_dict[hex_cmd.name] = hex_cmd
        
        
    def send_cmd(self, cmd_name):
        '''
        '''
        cmd_sent = False

        if cmd_name in self.cmd_dict:

            cmd_bytes = bytes(self.cmd_dict[cmd_name].data)    
            try:
                self.socket.sendto(cmd_bytes, self.cmd_ip_address)
                cmd_text = cmd_bytes.hex()
                cmd_sent = True
            except:
                cmd_text = "Failed to send command on socket to %s:%d" % self.cmd_ip_address

        else:
            cmd_text = "Command %s not registered with CmdSender" % cmd_name
            
        return (cmd_sent, cmd_text)


def main():
    
    cmd_manager = CmdManager('127.0.0.1',1234)

    cmd_hex = [0x18, 0x84, 0xC0, 0x00, 0x00, 0x01, 0x00, 0x00]
    cmd_sender.add_cmd(HexCmd('CI_LAB_NOOP', cmd_hex))
    
    cmd_hex = [0x18, 0x84, 0xC0, 0x00, 0x00, 0x01, 0x00, 0x00]
    cmd_sender.add_cmd(HexCmd('TO_LAB_NOOP', cmd_hex))

    cmd_hex = [0x18, 0x84, 0xC0, 0x00, 0x00, 0x01, 0x00, 0x00]
    cmd_sender.add_cmd(HexCmd('KIT_TO_NOOP', cmd_hex))

    cmd_hex = [0x1F, 0x20, 0xC0, 0x00, 0x00, 0x11, 0x00, 0x05, 
               0x27, 0x31, 0x32, 0x37, 0x2E, 0x30, 0x2E, 0x30, 
               0x2E, 0x31, 0x27, 0x00, 0x00, 0x00, 0x00, 0x00]
    cmd_sender.add_cmd(HexCmd('KIT_TO_ENABLE_OUTPUT', cmd_hex))

    cmd_hex = [0x18, 0x84, 0xC0, 0x00, 0x00, 0x01, 0x00, 0x00]
    cmd_sender.add_cmd(HexCmd('CF_NOOP', cmd_hex))

    cmd_Manager.send_cmd('CI_LAB_NOOP')
    print("cmd_sent %s:\n%s" % (str(cmd_sent),cmd_text))

if __name__ == "__main__":
    main()
    
