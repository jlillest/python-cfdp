import logging

CCSDS_PACKET_VERSION = 1
CCSDS_CMD_PKT_TYPE = 1
CCSDS_TLM_PKT_TYPE = 0
CCSDS_COMPLETE_SEG_FLAG = 3

logger = logging.getLogger(__name__)


class CcsdsHeader:
    """
    /*----- CCSDS packet primary header. -----*/

    typedef struct {

        uint8   StreamId[2];  /* packet identifier word (stream ID) */
            /*  bits  shift   ------------ description ---------------- */
            /* 0x07FF    0  : application ID                            */
            /* 0x0800   11  : secondary header: 0 = absent, 1 = present */
            /* 0x1000   12  : packet type:      0 = TLM, 1 = CMD        */
            /* 0xE000   13  : CCSDS version:    0 = ver 1, 1 = ver 2    */

        uint8   Sequence[2];  /* packet sequence word */
            /*  bits  shift   ------------ description ---------------- */
            /* 0x3FFF    0  : sequence count                            */
            /* 0xC000   14  : segmentation flags:  3 = complete packet  */

        uint8  Length[2];     /* packet length word */
           /*  bits  shift   ------------ description ---------------- */
           /* 0xFFFF    0  : (total packet length) - 7                 */

    } CCSDS_PriHdr_t;
    """

    def __init__(
            self,
            app_id=None,
            sec_hdr=0,
            pkt_type=None,
            seq_cnt=0,
            seg_flag=CCSDS_COMPLETE_SEG_FLAG,
            pkt_len=None):

        if None in [app_id, pkt_type, pkt_len]:
            raise Exception("Must provide all required parameters")

        self.app_id   = app_id
        self.sec_hdr  = sec_hdr
        self.pkt_type = pkt_type
        self.version  = CCSDS_PACKET_VERSION

        self.seq_cnt  = seq_cnt
        self.seg_flag = seg_flag
        
        self.pkt_len  = pkt_len

    def encode(self):
        
        #if self.pdu_data_field_length is None:
        #    raise ValueError("PDU data field length not defined")
            
        databytes = bytes([
            # TODO - Make portable endian solution


            ((self.app_id >> 8) & 0x07) | 
            ((self.sec_hdr  & 0x01) << 3) |
            ((self.pkt_type & 0x01) << 4) |
            ((self.version  & 0x0E) << 5),
            
            (self.app_id & 0xFF),

            ((self.seq_cnt >> 8) & 0x3F) |
            ((self.seg_flag & 0x03) << 6),

            (self.seq_cnt & 0xFF),
            
            ((self.pkt_len >> 8) & 0xFF),
            (self.pkt_len & 0xFF)])
            
        logger.debug("Header::encode()-databytes len %d, pkt_len %d" % (len(databytes),self.pkt_len))
        logger.debug("Header::encode()-databytes:" + str(databytes))

        return databytes

    @classmethod
    def decode(cls, pdu):
        pass
