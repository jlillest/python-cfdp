import socket
import time

class HexCmd():
    '''
    '''

    def __init__(self, name, data):
        
        self.name = name
        self.data = self.add_checksum(data)
        
            
    def add_checksum(self, data):
        '''
        Compute and insert the checksum for an array of command bytes. 
        '''
        data[6] = 0x00
        checksum = 0xFF
        for hex_byte in data:
            checksum = checksum ^ hex_byte
    
        data[6] = checksum
        
        return data
        
        
class CmdManager:
    '''
    Send hardcoded commands to a cFS target
    ''' 

    cmd_dict = {}
    
    def __init__(self, host_addr, cmd_port):
    
        self.cmd_ip_address = (host_addr, cmd_port) 
        
        try:
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        except:
            print("Error creating CmdSender socket")


    def add_cmd(self, hex_cmd: HexCmd):
        self.cmd_dict[hex_cmd.name] = hex_cmd
        
        
    def send_cmd(self, cmd_name):
        '''
        '''
        cmd_sent = False

        if cmd_name in self.cmd_dict:

            cmd_bytes = bytes(self.cmd_dict[cmd_name].data)    
            try:
                self.socket.sendto(cmd_bytes, self.cmd_ip_address)
                cmd_text = cmd_name + ': ' + cmd_bytes.hex()
                cmd_sent = True
            except:
                cmd_text = "Failed to send command %s on socket to %s:%d" % (cmd_name,self.cmd_ip_address)

        else:
            cmd_text = "Command %s not registered with CmdSender" % cmd_name
            
        return (cmd_sent, cmd_text)




def create_cmd_database(cmd_manager: CmdManager):


    cmd_hex = [0x18, 0x84, 0xC0, 0x00, 0x00, 0x01, 0x00, 0x00]
    cmd_manager.add_cmd(HexCmd('CI_LAB_NOOP', cmd_hex))
    
    cmd_hex = [0x18, 0x80, 0xC0, 0x00, 0x00, 0x01, 0x00, 0x00]
    cmd_manager.add_cmd(HexCmd('TO_LAB_NOOP', cmd_hex))

    cmd_hex = [0x18, 0x80, 0xC0, 0x00, 0x00, 0x11, 0x00, 0x06, 
               0x31, 0x32, 0x37, 0x2E, 0x30, 0x2E, 0x30, 0x2E, 
               0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
    cmd_manager.add_cmd(HexCmd('TO_LAB_ENABLE_OUTPUT', cmd_hex))

    cmd_hex = [0x1F, 0x20, 0xC0, 0x00, 0x00, 0x01, 0x00, 0x00]
    cmd_manager.add_cmd(HexCmd('KIT_TO_NOOP', cmd_hex))

    cmd_hex = [0x1F, 0x20, 0xC0, 0x00, 0x00, 0x11, 0x00, 0x05, 
               0x31, 0x32, 0x37, 0x2E, 0x30, 0x2E, 0x30, 0x2E, 
               0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
    cmd_manager.add_cmd(HexCmd('KIT_TO_ENABLE_OUTPUT', cmd_hex))

    cmd_hex = [0x18, 0xB3, 0xC0, 0x00, 0x00, 0x01, 0x00, 0x00]
    cmd_manager.add_cmd(HexCmd('CF_NOOP', cmd_hex))

    '''
    COMMAND CF PLAYBACK_FILE <%= Osk::Cfg.processor_endian %> "Put the specified file in the playback queue"
      <%= Osk::Cfg.cmd_hdr(@APP_PREFIX_STR, @CMD_MID_STR, 2, 148) %>
      APPEND_PARAMETER CLASS           8 UINT MIN_UINT8 MAX_UINT8 1 "Class_1(1), Class_2(2)"
      APPEND_PARAMETER CHANNEL         8 UINT MIN_UINT8 MAX_UINT8 0 "Chan_0(0), Chan_1(1)"
      APPEND_PARAMETER PRIORITY        8 UINT MIN_UINT8 MAX_UINT8 0 "Priority – 00 (highest priority) – 0xff (lowest priority)"
      APPEND_PARAMETER PRESERVE        8 UINT MIN_UINT8 MAX_UINT8 1 "Delete_File(0), Keep_File(1)"
      APPEND_PARAMETER PEER_ID       128 STRING <%= Osk::CFDP_GND_ENTITY_ID %> "Entity ID of ground engine to receive file (ex 0.23)"
      APPEND_PARAMETER SRC_FILENAME  512 STRING <%= SimSat::CFDP_FLT_DIR %> "Complete target /path/filename"
      APPEND_PARAMETER DEST_FILENAME 512 STRING <%= SimSat::CFDP_GND_DIR %> "GND dir relative to COSMOS base dir"
    '''
    cmd_hex = [0x18, 0xB3, 0xC0, 0x00, 0x00, 0x95, 0x00, 0x02,
               0x01, 0x00, 0x00, 0x01, 
               0x30, 0x2E, 0x32, 0x31, 0x00, 0x00, 0x00, 0x00,  #0.21
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x2F, 0x63, 0x66, 0x2F, 0x61, 0x61, 0x5F, 0x6C,  #/cf/aa_libre.txt
               0x69, 0x62, 0x72, 0x65, 0x2E, 0x74, 0x78, 0x74,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x61, 0x61, 0x5F, 0x6C, 0x69, 0x62, 0x72, 0x65,  #aa_libre.txt
               0x2E, 0x74, 0x78, 0x74, 0x00, 0x00, 0x00, 0x00,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
               0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00]
    cmd_manager.add_cmd(HexCmd('CF_PLAYBACK_FILE', cmd_hex))



def main():
    
    cmd_manager = CmdManager('127.0.0.1',1234)

    create_cmd_database(cmd_manager)

    #cmd_sent, cmd_text = cmd_manager.send_cmd('TO_LAB_ENABLE_OUTPUT')
    cmd_sent, cmd_text = cmd_manager.send_cmd('KIT_TO_ENABLE_OUTPUT')
    print("cmd_sent %s:\n%s" % (str(cmd_sent),cmd_text))

    time.sleep(0.5)
    
    cmd_sent, cmd_text = cmd_manager.send_cmd('CF_NOOP')
    print("cmd_sent %s:\n%s" % (str(cmd_sent),cmd_text))

    time.sleep(0.5)
    
    cmd_sent, cmd_text = cmd_manager.send_cmd('CF_PLAYBACK_FILE')
    print("cmd_sent %s:\n%s" % (str(cmd_sent),cmd_text))

if __name__ == "__main__":
    main()
    
