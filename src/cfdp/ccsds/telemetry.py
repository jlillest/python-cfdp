from .header import CcsdsHeader, CCSDS_TLM_PKT_TYPE

#
# CCSDS Packet Length = total packet length - 7.
#    Telemetry packets have 12 bytes of header so add 5 to payload length
#
class CcsdsTelemetry(CcsdsHeader):
    '''
    /*----- CCSDS telemetry secondary header. -----*/

    typedef struct {

       uint8  Time[CCSDS_TIME_SIZE];

    } CCSDS_TlmSecHdr_t;
    '''
    
    def __init__(
            self,
            app_id=None,
            payload=None):
        
        if None in [app_id, payload]:
            raise Exception("Must provide all required parameters")
        
        self.seconds     = 0 # 4 bytes
        self.subseconds  = 0 # 2 bytes
        self.pkt_len   = 0 if payload is None else (5 + len(payload))
        
        super().__init__(
            app_id=app_id,
            sec_hdr=1,
            pkt_type=CCSDS_TLM_PKT_TYPE,
            pkt_len=self.pkt_len)


    def encode(self):
        databytes = super().encode()
        databytes += bytes([
            (self.seconds & 0x000000FF),
            (self.seconds & 0x0000FF00) >>  8,
            (self.seconds & 0x00FF0000) >> 16,
            (self.seconds & 0xFF000000) >> 24,
            (self.subseconds & 0x000000FF),
            ((self.subseconds & 0x0000FF00) >> 8)])

        return databytes

    @classmethod
    def decode(cls, pdu):
        pass
        
