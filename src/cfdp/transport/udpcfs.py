import logging
import threading
import select
import time

from . import UdpTransport
from cfdp.ccsds import CcsdsCommand, CcsdsTelemetry

logger = logging.getLogger(__name__)

#
# CF App Constants that should be defined outside of this module
#
PDU_MID = 0x1FFD

class UdpCfsTransport(UdpTransport):

    def __init__(self):
        super().__init__()

    def request(self, data, address):
        logger.debug("**UdpCfsTransport::request(%s)" % address)
        # Wrap data in a CCSDS command packet
        ccsds_pkt = CcsdsCommand(PDU_MID,0,data)
        super().request(ccsds_pkt.encode(),address)
        time.sleep(0.5)
        
    def _incoming_pdu_handler(self):
        logger.debug("cFS --> incoming_pdu_handler()")
        thread = threading.currentThread()
        _socket_list = [self.socket]

        i = 0
        for x in self.config.remote_entities:
            logger.debug("remote_entity[%d] = %d" % (i,x.entity_id))
            i += 1
        # make sure that even the larges PDU will fit into the buffer
        maximum_file_segment_lengths = [
            self.config.get(x.entity_id).maximum_file_segment_length
            for x in self.config.remote_entities]
        if maximum_file_segment_lengths:
            buffer_size = 10 * max(maximum_file_segment_lengths)
        else:
            buffer_size = 10 * DEFAULT_BUFFER_SIZE

        while not thread.kill:
            try:
                readable, _, _ = select.select(_socket_list, [], [], 0)
            except ValueError:
                break

            for sock in readable:
                data, addr = sock.recvfrom(buffer_size)
                data = data[12:]  #Remove CCSDS telemetry header
                self.indication(data)        
